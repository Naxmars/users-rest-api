<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 12.5.17
 * Time: 20:05
 */

namespace App\Model;

class Users {

    /** @var $em \Kdyby\Doctrine\EntityManager */
    public $em;

    /** @var $users \Kdyby\Doctrine\EntityRepository */
    public $users;

    /**
     * UsersPresenter constructor.
     * @param \Kdyby\Doctrine\EntityManager $em
     */
    public function __construct(\Kdyby\Doctrine\EntityManager $em) {
        $this->em = $em;
        $this->users = $em->getRepository(\App\Model\User::class);
    }

    public function read($id) {
        /** @var $entity \App\Model\User */
        $entity = $this->users->find($id);

        if($entity) {
            return [
                'id' => $entity->getId(),
                'name' => $entity->getName(),
                'surname' => $entity->getSurname(),
                'email' => $entity->getEmail(),
                'password' => $entity->getPassword(),
            ];
        }

        return false;
    }

    public function create(array $data) {
        $user = new User();
        if(!isset($data['email'], $data['name'], $data['surname'], $data['password'])) return false;

        $user->setEmail($data['email']);
        $user->setName($data['name']);
        $user->setSurname($data['surname']);
        $user->setPassword($data['password']);

        $this->em->persist($user);
        $this->em->flush();

        return true;
    }

    public function update($id, $data) {
        /** @var $entity \App\Model\User */
        $entity = $this->users->find($id);

        if(!$entity) return false;

        if(isset($data['email'])) $entity->setEmail($data['email']);
        if(isset($data['name'])) $entity->setName($data['name']);
        if(isset($data['surname'])) $entity->setSurname($data['surname']);
        if(isset($data['password'])) $entity->setPassword($data['password']);

        $this->em->persist($entity);
        $this->em->flush();

        return true;
    }

    public function delete($id) {
        $entity = $this->users->find($id);

        if($entity) {
            $this->em->remove($entity);
            $this->em->flush();
            return true;
        }

        return false;
    }

}