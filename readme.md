# Funkce
- GET request na http://host/api/users/<id> vrátí uživatele s id <id>, pokud existuje, ve formátu JSON    
- POST request na http://host/api/users/ vytvoří uživatele dle parametrů požadavku 
- DELETE request na http://host/api/users/<id> odstraní uživatele s id <id>, pokud existuje
- PUT request na http://host/api/users/<id> edituje uživatele s id <id>, pokud existuje, dle parametrů v těle požadavku